<?php

/**
 * Helper function for meal autocompletion
 */
function diet_meal_autocomplete($string = '') {
  global $user;
  
  /*  $matches = array();
    $result = db_query_range("SELECT n.nid, n.title FROM {node} n LEFT JOIN {diet_meal} m ON m.nid = n.nid WHERE n.type = 'meal' AND n.status = 1  AND m.energy != 0 AND LOWER(n.title) LIKE LOWER('%%%s%%') AND m.uid = 0 ORDER BY n.title", $string, 0, 50);
    $prefix = count($array) ? implode(', ', $array) .', ' : '';
    while ($meal = db_fetch_object($result)) {
      $meal->title = tt("meal:node_" . $meal->nid, $meal->title);
      $n = $meal->title;      
      $matches[$n . ' - [' . $meal->nid .']'] = check_plain($meal->title);
    }
    */
    $matches = array();
     $result = db_query("SELECT n.nid, n.title, m.uid FROM {node} n LEFT JOIN {diet_meal} m ON m.nid = n.nid WHERE n.type = 'meal' AND n.status = 1 AND m.energy != 0 AND LOWER(n.title) LIKE LOWER('%%%s%%') AND (m.uid = 0 || m.uid = %d) ORDER BY n.title", $string, $user->uid);
     $prefix = count($array) ? implode(', ', $array) .', ' : '';
     while ($meal = db_fetch_object($result)) {
       $meal->title = tt("meal:node_" . $meal->nid, $meal->title);
       $n = $meal->title;   
       $u = $meal->uid != 0 ? ' <strong><em>' . t('User meal') . '</em></strong>': '';   
       $matches[$n . ' - [' . $meal->nid .']'] = check_plain($meal->title) . $u;
     }
  drupal_json($matches);
}

function diet_user_meal_autocomplete($string = '') {
  global $user;
    $matches = array();
    $result = db_query_range("SELECT n.nid, n.title FROM {node} n LEFT JOIN {diet_meal} m ON m.nid = n.nid WHERE n.type = 'meal' AND n.status = 1  AND m.energy != 0 AND LOWER(n.title) LIKE LOWER('%%%s%%') AND m.uid = %d ORDER BY n.title", $string, $user->uid, 0, 50);
    $prefix = count($array) ? implode(', ', $array) .', ' : '';
    while ($meal = db_fetch_object($result)) {
      $meal->title = tt("meal:node_" . $meal->nid, $meal->title);
      $n = $meal->title;      
      $matches[$n . ' - [' . $meal->nid .']'] = check_plain($meal->title);
    }
  drupal_json($matches);
}
