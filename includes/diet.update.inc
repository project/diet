<?php

/**
 * updates food items from food database.
 *
 */
function diet_update_food() {
  $fids = diet_get_food_to_update();
  $output = t('There are @count food items to update', array('@count' => count($fids)));
  $output .= drupal_get_form('diet_update_food_form');

  return $output;
}

/**
 * get missing food items to update.
 *
 * @return array
 *   values are idAliments to update.
 */
function diet_get_food_to_update() {
  $sql = "SELECT IDAliment as fid from {food_ddbb} WHERE IDAliment NOT IN (SELECT IDAliment FROM {diet_food})";
  $result = db_query($sql);
  while ($fid = db_fetch_object($result)) {
    $fids[] = $fid->fid;
  }
  return $fids;
}

/**
 * print food update form.
 *
 */
function diet_update_food_form() {
  $fids = diet_get_food_to_update();
  $disabled = count($fids) == 0 ? true : false;
  $value = count($fids) == 0 ? t('There are not updates available') : t('click the button to update the missing food items to the node system.');
  $form['food_update'] = array(
    '#type' => 'item',
    '#title' => t('Update food items'),
    '#value' => $value,
  );

  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Update'),
    '#disabled' => $disabled,
  );
  return $form;
}

/**
 * processes the races update form.
 *
 */
function diet_update_food_form_submit($form_id, $form_values) {
    $fids = diet_get_food_to_update();
    $i = 0;
    $count = count($fids);
    //var_dump($rids);
    foreach ($fids as $fid) {
      $i++;
      $result = db_query('SELECT NomAliment FROM {food_ddbb} WHERE IDAliment = %d', $fid);
      $node->nid = '';
      $node->title = db_result($result); 
      $node->type = 'food';
      $node->status = 1;
      $node->uid = 1;
      $node->promote = 0;
      $node->comment = 0;
      $node->created = time() - $count + $i;
      $node->changed = time() - $count + $i;
      $node->IDAliment = (int)$fid;
      $node->diet_food_update = 1;
      //var_dump($node);
      node_save($node);
      unset($node);
    }
}

