<?php

/**
 * @file ingestion.inc
 * Provides the ingestion object type and associated methods.
 */

/**
 * An object to contain all the ingestion data and process it
 */
class ingestion {
  var $date = 0;
  var $meals = array();
  var $type = '';
  var $uid = 0;
  function __construct() {
    $this->meals = array();
  }
  function set_date($date) {
    $this->date = $date;
  }

  function set_uid($uid) {
    $this->uid = $uid;
  }

  function set_type($type) {
    $this->type = $type;
  }

  /**
   * Save the first part of an ingestion creation form
   * The ingestion object needs to have set the date, uid and type.
   *
   * @return
   *   int. nid.
   */
  function save_form_1() {
    global $user;
    $node->title = format_date(strtotime($this->date), 'custom', 'l, d/M/Y') .' - '. _diet_get_ingestion_type_name($this->type);
    $node->type = 'ingestion';
    $node->uid = $user->uid;
    $node->created = time();
    $node->ingestion['date'] = $this->date;
    $node->ingestion['uid'] = $this->uid;
    $vid = _diet_get_ingestion_type_vid();
    $node->taxonomy[$vid] = $this->type;
    //var_dump($node); die;
    node_save($node);
    return $node->nid;
  }
  
  /**
   * loads an ingestion given an ingestion id
   *
   * @param $iid
   *   int. ingestion id.
   * @return
   *   object.
   */
  function load($iid) {
    $ingestion = new ingestion();
    $ingestion->date = db_result(db_query("SELECT date FROM {diet_ingestion} WHERE nid = %d", $iid));
    $ingestion->uid = db_result(db_query("SELECT uid FROM {diet_ingestion} WHERE nid = %d", $iid));
    $result = db_query("SELECT mid FROM {diet_meal_ingestion} WHERE iid = %d", $iid);
    while ($row = db_fetch_object($result)) {
      $meals[$row->mid] = $row->mid;
    }
    $ingestion->meals = $meals;
    $ingestion->type = $this->get_type($iid);
    $ingestion->nid = $iid;
    //var_dump($meals);
    return $ingestion;
  }
  
  /**
   * gets type of ingestion given an ingestion id
   *
   * @param $iid
   *   int. ingestion id.
   * @return
   *   array.
   */
   function get_type($iid) {
     $vid = _diet_get_ingestion_type_vid();
     return db_fetch_array(db_query("SELECT tn.tid, td.name FROM {term_node} tn LEFT JOIN {term_data} td ON tn.tid = td.tid WHERE tn.nid = %d AND td.vid = %d", $iid, $vid));
   }
   
   /**
    * returns a list of meals for a given ingestion
    *
    * @param $iid
    *   int. ingestion id.
    * @return
    *   array.
    */
    function get_meals($iid) {
      $meal_list = array();
       $result = db_query("SELECT * FROM {diet_meal_ingestion} WHERE iid = %d", $iid);
       while ($row = db_fetch_array($result)) {
         $meal_list[] = $row;
       } 
       return $this->meals = $meal_list;
    }
}

