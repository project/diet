<?php

/**
 * Helper function for food autocompletion
 */
function diet_food_autocomplete($string = '') {
    $matches = array();
    $result = db_query_range("SELECT n.nid, n.title FROM {node} n WHERE n.type = 'food' AND LOWER(n.title) LIKE LOWER('%%%s%%') ORDER BY n.title", $string, 0, 100);
    $prefix = count($array) ? implode(', ', $array) .', ' : '';
    while ($food = db_fetch_object($result)) {
      $food->title = tt("food:node_" . $food->nid, $food->title);
      $n = $food->title;      
      $matches[$n . ' - [' . $food->nid . ']'] = check_plain($food->title);
    }
  drupal_json($matches);
}
