<?php

/**
 * Helper function for meal autocompletion
 */
function diet_ingestion_user_autocomplete($string = '') {
    $matches = array();
    $result = db_query_range("SELECT u.name FROM {users} u INNER JOIN {diet_ingestion} i ON i.uid = u.uid WHERE LOWER(u.name) LIKE LOWER('%%%s%%') ORDER BY u.name", $string, 0, 50);
    $prefix = count($array) ? implode(', ', $array) .', ' : '';
    while ($user = db_fetch_object($result)) {
      $n = $user->name;      
      $matches[$prefix . $n] = check_plain($user->name);
    }
  drupal_json($matches);
}