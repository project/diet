THIS IS NOT A READY FOR PRODUCTION SCRIPT!

This is a module we are using at http://predicam.cat for a clinical study at the Sant pau Hospital of Barcelona. It was written before ctools was out so there are many thinks which could be improved. Also, it needs many thinks to get generalized as they are quite specific for the hospital. Thus, this might not be of much help for you if you don't have some knowledge on drupal programming, I'm afraid.

However, I wanted to make this script available to anyone who might need such functionality. Please, use the issue queue on http://drupal.org/project/issues/diet for any issue you might have.

