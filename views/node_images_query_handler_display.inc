<?php
/**
 * Field handler to provide simple renderer that allows using a themed user link
 */
class node_images_query_handler_display extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();

    $options['size'] = array('default' => 'thumbnail');

    return $options;
  }
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['size'] = array(
      '#type' => 'select',
      '#title' => t('Output size'),
      '#options' => array(
        'thumbs' => t('Thumbnail'),
        'fullsize' => t('Full size'),
      ),
      '#default_value' => $this->options['size'],
    );
   
  }
  
  function render($values) {
    $value = $values->{$this->field_alias};
    if ($value) {
      //TODO put this in a theme function
      $image = db_fetch_object(db_query('SELECT * FROM {node_images} WHERE id=%d', $value));
      $description = check_plain($image->description);
      $pattern = '<img src="%path" alt="%description" />';
      $thumb = strtr($pattern, array('%path'=>file_create_url($image->thumbpath), '%description'=>$description));
      
      return '<a href="javascript:void(0);" title="' . $description 
            . '" onclick="window.open(\'' . file_create_url($image->filepath) 
            . '\', \'\', \'height=' . $height . ',width=' . $width . '\');">' . $thumb . '</a> ';
    }
    
    
  }
}

