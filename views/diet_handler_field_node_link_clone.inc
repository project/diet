<?php

/**
* Field handler to present a clone meal node link.
*
*/
class diet_handler_field_node_link_clone extends views_handler_field_node_link {
  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = 'nid';
  //  $this->additional_fields['type'] = 'type';
  //  $this->additional_fields['format'] = array('table' => 'node_revisions', 'field' => 'format');
  }

  function render($values) {
    // Insure that user has access to clone this node.
   // $node = new stdClass();
   // $node->nid = $values->{$this->aliases['nid']};
    //var_dump($values);die;
    //$node->uid = $values->{$this->aliases['uid']};
    //$node->type = $values->{$this->aliases['type']};
    //$node->format = $values->{$this->aliases['format']};
    //$node->status = 1; // unpublished nodes ignore access control
    //if (!user_access('admin diet modle')) {
    //  return;
   // }

    $text = !empty($this->options['text']) ? $this->options['text'] : t('Clone meal');
    return l($text, "node/$values->nid/clone", array('query' => drupal_get_destination()));
  }
}
