<?php

/**
 * Implementation of hook_views_default_views().
 */
function diet_views_default_views() {
  $view = new view;
  $view->name = 'food_items';
  $view->description = t('List of foods');
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $vft = _diet_get_food_type_vid();
  $handler->override_option('fields', array(
    'title' => array(
      'label' => t('Food'),
      'link_to_node' => 1,
      'exclude' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'none',
      'override' => array(
        'button' => 'Override',
      ),
    ),
  'tid' => array(
    'label' => t('All terms'),
    'type' => 'separator',
    'separator' => ', ',
    'empty' => '',
    'link_to_taxonomy' => 1,
    'limit' => 0,
    'vids' => array(
      '$vft' => $vft,
    ),
    'exclude' => 0,
    'id' => 'tid',
    'table' => 'term_node',
    'field' => 'tid',
    'override' => array(
      'button' => 'Override',
    ),
    'relationship' => 'none',
  ),
    'Energia' => array(
      'label' => t('Energy'),
      'exclude' => 0,
      'id' => 'Energia',
      'table' => 'food_ddbb',
      'field' => 'Energia',
      'relationship' => 'none',
    ),
    'Glucidos_totales' => array(
      'label' => t('Glucose'),
      'exclude' => 0,
      'id' => 'Glucidos_totales',
      'table' => 'food_ddbb',
      'field' => 'Glucidos_totales',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'Lipidos_totales' => array(
      'label' => t('Lipids'),
      'exclude' => 0,
      'id' => 'Lipidos_totales',
      'table' => 'food_ddbb',
      'field' => 'Lipidos_totales',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'Proteina_total' => array(
      'label' => t('Protein'),
      'exclude' => 0,
      'id' => 'Proteina_total',
      'table' => 'food_ddbb',
      'field' => 'Proteina_total',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'edit_node' => array(
      'label' => '',
      'text' => '',
      'exclude' => 0,
      'id' => 'edit_node',
      'table' => 'node',
      'field' => 'edit_node',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'gr' => array(
      'label' => t('Weight'),
      'exclude' => 0,
      'id' => 'gr',
      'table' => 'diet_food_measures',
      'field' => 'gr',
      'relationship' => 'none',
    ),
    'node_images_display' => array(
      'label' => t('Image'),
      'size' => 'thumbs',
      'exclude' => 0,
      'id' => 'node_images_display',
      'table' => 'measure_images',
      'field' => 'node_images_display',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'food' => 'food',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
    'title' => array(
      'operator' => 'contains',
      'value' => '',
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 1,
        'operator' => 'title_op',
        'identifier' => 'title',
        'label' => t('Food'),
        'optional' => 1,
        'remember' => 0,
      ),
      'case' => 1,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'language' => array(
      'operator' => 'in',
      'value' => array(
        '***CURRENT_LANGUAGE***' => '***CURRENT_LANGUAGE***',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'language',
      'table' => 'node',
      'field' => 'language',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'role',
    'role' => array(
      '9' => 9,
    ),
  ));
  $handler->override_option('title', t('Foods list'));
  $handler->override_option('header', t("Note:<br/>Values between (): estimated values.<br/>Values between []: estimated values from its ingredients."));
  $handler->override_option('header_format', '1');
  $handler->override_option('header_empty', 0);
  $handler->override_option('items_per_page', 50);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
      'title' => 'title',
      'Energia' => 'Energia',
      'Glucidos_totales' => 'Glucidos_totales',
      'Lipidos_totales' => 'Lipidos_totales',
      'Proteina_total' => 'Proteina_total',
    ),
    'info' => array(
      'title' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'Energia' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'Glucidos_totales' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'Lipidos_totales' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'Proteina_total' => array(
        'sortable' => 1,
        'separator' => '',
      ),
    ),
    'default' => 'title',
  ));
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->override_option('path', 'food');
  $handler->override_option('menu', array(
    'type' => 'normal',
    'title' => 'Food list',
    'weight' => '-10',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'weight' => 0,
  ));
  
  $views['food_items'] = $view;
  
  //********************//
  $view = new view;
  $view->name = 'meal_list';
  $view->description = t('List of meals');
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $vmg = _diet_get_meal_group_vid();
  $vmt = _diet_get_meal_type_vid();
  $handler->override_option('fields', array(
    'title' => array(
      'label' => t('Title'),
      'link_to_node' => 1,
      'exclude' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'none',
    ),
    'tid' => array(
      'label' => t('All terms'),
      'type' => 'separator',
      'separator' => ', ',
      'empty' => '',
      'link_to_taxonomy' => 1,
      'limit' => 0,
      'vids' => array(
        '$vmg' => $vmg,
        '$vmt' => $vmt,
      ),
      'exclude' => 0,
      'id' => 'tid',
      'table' => 'term_node',
      'field' => 'tid',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'edit_node' => array(
      'id' => 'edit_node',
      'table' => 'node',
      'field' => 'edit_node',
    ),
    'node_images_display' => array(
      'label' => t('Image'),
      'size' => 'thumbs',
      'exclude' => 0,
      'id' => 'node_images_display',
      'table' => 'meal_images',
      'field' => 'node_images_display',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'meal' => 'meal',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
    'status' => array(
      'operator' => '=',
      'value' => 1,
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('title', t('Meal list'));
  $handler->override_option('empty', t('There are no results with this filters criteria. Please, try a different one.'));
  $handler->override_option('empty_format', '1');
  $handler->override_option('items_per_page', 50);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
      'title' => 'title',
    ),
    'info' => array(
      'title' => array(
        'sortable' => 0,
        'separator' => '',
      ),
    ),
    'default' => '-1',
  ));
  $handler = $view->new_display('page', 'Page', 'page_1');
 
  $handler->override_option('filters', array(
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'meal' => 'meal',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
    'status' => array(
      'operator' => '=',
      'value' => 1,
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'relationship' => 'none',
    ),
    'tid' => array(
      'operator' => 'or',
      'value' => '',
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'tid_op',
        'identifier' => 'tid',
        'label' => t('Filter by Meal group (pasta, salad, etc)'),
        'optional' => 1,
        'single' => 1,
        'remember' => 0,
      ),
      'type' => 'textfield',
      'vid' => $vmg,
      'id' => 'tid',
      'table' => 'term_node',
      'field' => 'tid',
      'hierarchy' => 0,
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
      'reduce_duplicates' => 0,
    ),
    'tid_1' => array(
      'operator' => 'or',
      'value' => '',
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'tid_1_op',
        'identifier' => 'tid_1',
        'label' => t('Filter by Meal type (first course, second course, etc)'),
        'optional' => 1,
        'single' => 1,
        'remember' => 0,
      ),
      'type' => 'textfield',
      'vid' => $vmt,
      'id' => 'tid_1',
      'table' => 'term_node',
      'field' => 'tid',
      'hierarchy' => 0,
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
      'reduce_duplicates' => 0,
    ),
  ));
  $handler->override_option('path', 'meals');
  $handler->override_option('menu', array(
    'type' => 'normal',
    'title' => 'Meals list',
    'weight' => '-9',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'weight' => 0,
  ));
  
  
  $views['meal_list'] = $view;
  
  /********* ingestion list **************/
  
  $view = new view;
  $view->name = 'ingestion_list';
  $view->description = 'List of ingestions';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'title' => array(
      'label' => t('Ingestion'),
      'link_to_node' => 1,
      'exclude' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'none',
    ),
    'uid' => array(
      'label' => t('User'),
      'link_to_user' => 1,
      'exclude' => 0,
      'id' => 'uid',
      'table' => 'diet_ingestion',
      'field' => 'uid',
      'relationship' => 'none',
    ),
    'tid' => array(
      'label' => t('Type'),
      'type' => 'separator',
      'separator' => ', ',
      'empty' => '',
      'link_to_taxonomy' => 1,
      'limit' => 0,
      'vids' => array(
        '$vmg' => '7',
        '$vmt' => '8',
      ),
      'exclude' => 0,
      'id' => 'tid',
      'table' => 'term_node',
      'field' => 'tid',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'date' => array(
      'label' => '',
      'date_format' => 'custom',
      'custom_date_format' => 'd-m-y',
      'exclude' => 1,
      'id' => 'date',
      'table' => 'diet_ingestion',
      'field' => 'date',
      'relationship' => 'none',
    ),
    'edit_ingestion' => array(
        'label' => t('Edit ingestion link'),
        'text' => t('Edit meals'),
        'exclude' => 0,
        'id' => 'edit_ingestion',
        'table' => 'diet_ingestion',
        'field' => 'edit_ingestion',
        'relationship' => 'none',
      ),
  ));
  $handler->override_option('sorts', array(
    'date' => array(
      'order' => 'DESC',
      'granularity' => 'day',
      'id' => 'date',
      'table' => 'diet_ingestion',
      'field' => 'date',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('arguments', array(
    'uid' => array(
      'default_action' => 'ignore',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'Todos',
      'title' => '',
      'default_argument_type' => 'fixed',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'not found',
      'break_phrase' => 0,
      'not' => 0,
      'id' => 'uid',
      'table' => 'users',
      'field' => 'uid',
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_user' => 0,
      'default_argument_fixed' => '',
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'webform' => 0,
        'image' => 0,
        'ingestion' => 0,
        'food' => 0,
        'food_measure' => 0,
        'meal' => 0,
        'food_cart' => 0,
        'diet' => 0,
        'multichoice' => 0,
        'quiz' => 0,
        'forum' => 0,
        'panel' => 0,
        'analitica' => 0,
        'analitica_seguimiento' => 0,
        'book' => 0,
        'opcions_usuari' => 0,
        'page' => 0,
        'profile' => 0,
        'story' => 0,
        'ttog' => 0,
        'visita_1' => 0,
        //TODO generalize this view
        'visita_10_grupo_3' => 0,
        'visita_10_grupo_4' => 0,
        'visita_2' => 0,
        'visita_3' => 0,
        'visita_4_grupo_1' => 0,
        'visita_4_grupo_2' => 0,
        'visita_4_grupo_3' => 0,
        'visita_4_grupo_4' => 0,
        'visita_5_grupo_3' => 0,
        'visita_5_grupo_4' => 0,
        'visita_6_grupo_3' => 0,
        'visita_6_grupo_4' => 0,
        'visita_7_grupo_3' => 0,
        'visita_7_grupo_4' => 0,
        'visita_8_grupo_3' => 0,
        'visita_8_grupo_4' => 0,
        'visita_9_grupo_3' => 0,
        'visita_9_grupo_4' => 0,
        'visita_final_grupo_1' => 0,
        'visita_final_grupo_2' => 0,
        'visita_final_grupo_3' => 0,
        'visita_final_grupo_4' => 0,
        'visita_seguimiento_grupo_2' => 0,
        'visita_seguimiento_grupo_3' => 0,
        'visita_seguimiento_grupo_4' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(
        '4' => 0,
        '9' => 0,
        '5' => 0,
        '6' => 0,
        '7' => 0,
        '8' => 0,
        '2' => 0,
        '10' => 0,
        '1' => 0,
      ),
      'validate_argument_type' => 'tid',
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('filters', array(
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'ingestion' => 'ingestion',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
    'status' => array(
      'operator' => '=',
      'value' => 1,
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('title', t('Ingestion list'));
  $handler->override_option('empty', t('There are no results with this filters criteria. Please, try a different one.'));
  $handler->override_option('empty_format', '1');
  $handler->override_option('items_per_page', 50);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => 'date',
    'override' => 1,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
      'title' => 'title',
      'uid' => 'uid',
      'tid' => 'tid',
      'date' => 'date',
      'edit_node' => 'edit_node',
    ),
    'info' => array(
      'title' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'uid' => array(
        'separator' => '',
      ),
      'tid' => array(
        'separator' => '',
      ),
      'date' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'edit_node' => array(
        'separator' => '',
      ),
    ),
    'default' => '-1',
  ));
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->override_option('path', 'ingestions');
  $handler->override_option('menu', array(
    'type' => 'normal',
    'title' => 'Ingestions list',
    'weight' => '-8',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'weight' => 0,
  ));
  
  
  $views['ingestion_list'] = $view;
  
  return $views;
}