<?php
/**
 * Field handler to provide simple renderer that allows using a themed user link
 */
class diet_handler_field_user_name extends views_handler_field_user {
  function render_link($data, $values) {
    if (!empty($this->options['link_to_user'])) {
      $account = new stdClass();
      $account->name = db_result(db_query("SELECT name FROM {users} WHERE uid = %d", $values->{$this->field_alias}));
      $account->uid = $values->{$this->field_alias};
      return theme('username', $account);
    }
    else {
      return $data;
    }
  }
}

