<?php

/**
 * Implementation of hook_views_data().
 */
function diet_views_data() {
  //include(drupal_get_path('module', 'views') . '/handlers/views_handler_field.inc');
  //include(drupal_get_path('module', 'diet') . '/includes/views_handler_field_meal_energy.inc');
  
  // table diet_food
  $data['diet_food']['table']['group'] = t('Food');
  $data['diet_food']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field'=> 'nid',
    ),
  );
  
  // diet_food fields

  
  
  //table food_ddbb
  $data['food_ddbb']['table']['group'] = t('Food');
  $data['food_ddbb']['table']['join'] = array(
    'node' => array(
      'left_table' => 'diet_food',
      'left_field' => 'IDAliment',
      'field'=> 'IDAliment',
    ),
  );
    
  //food_ddbb fields
  $data['food_ddbb']['Energia'] = array(
    'title' => t('Energy'),
    'help' => t('the energy value of the food'),
    'field' => array(
        //'handler' => 'views_handler_field_node',
        'click sortable' => TRUE,
    ),
  );
  
  $data['food_ddbb']['Proteina_total'] = array(
    'title' => t('Protein'),
    'help' => t('the protein value of the food'),
    'field' => array(
        //'handler' => 'views_handler_field_node',
        'click sortable' => TRUE,
    ),
  );
  
  $data['food_ddbb']['Lipidos_totales'] = array(
    'title' => t('Lipids'),
    'help' => t('the lipids value of the food'),
    'field' => array(
        //'handler' => 'views_handler_field_node',
        'click sortable' => TRUE,
    ),
  );

  $data['food_ddbb']['Glucidos_totales'] = array(
    'title' => t('Glucose'),
    'help' => t('the glucose value of the food'),
    'field' => array(
        //'handler' => 'views_handler_field_node',
        'click sortable' => TRUE,
    ),
  );  
 
  // table diet_meal
  $data['diet_meal']['table']['group'] = t('Meal');
  $data['diet_meal']['table']['join'] = array(
   'node' => array(
     'left_table' => 'node',
     'left_field' => 'nid',
     'field'=> 'nid',
   ),
  );
  
  
  // diet_meal fields
  /*
  $data['diet_meal']['clone_meal'] = array(
    'real field' => 'nid',
    'title' => t('Clone meal'),
    'help' => t('A clone function'),
    'field' => array(
        'handler' => 'diet_handler_field_node_link_clone',
    ),
  );
  */
  
  // table meal_images
  $data['meal_images']['table']['group'] = t('Meal');
  $data['meal_images']['table']['join'] = array(
   'node' => array(
      'table' => 'node_images',
      'left_table' => 'diet_meal',
      'left_field' => 'nid',
      'field'=> 'nid',
    ),
  );
  $data['meal_images']['node_images_display'] = array(
    'real field'=> 'id',
    'title' => t('Images'),
    'help' => t('Image of a meal'),
    'field' => array(
        'handler' => 'node_images_query_handler_display',
    ),
  );
  

  //table diet_food_meal
  $data['diet_food_meal']['table']['group'] = t('Meal');
  $data['diet_food_meal']['table']['join'] = array(
   'node' => array(
     'left_table' => 'diet_meal',
     'left_field' => 'nid',
     'field'=> 'mid',
   ),
  );

  //food_ddbb fields
  $data['diet_food_meal']['energy'] = array(
    'title' => t('Energy'),
    'help' => t('The energy value of the meal'),
    'field' => array(
        'handler' => 'diet_handler_meal_energy',
    ),
  );
  
  //table diet_ingestion
  $data['diet_ingestion']['table']['group'] = t('Ingestion');
  $data['diet_ingestion']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field'=> 'nid',
    ),
  );
  
  //diet_ingestion fields
  $data['diet_ingestion']['uid'] = array(
    'title' => t('User'),
    'help' => t('The user this ingestion is assigned to.'),
    'field' => array(
        'handler' => 'diet_handler_field_user_name',
    ),
    'filter' => array(
        'handler' => 'diet_filter_field_user_name',
    ),
  );
  $data['diet_ingestion']['date'] = array(
    'title' => t('Date'),
    'help' => t('Date of the ingestion.'),
    'field' => array(
        'handler' => 'views_handler_field_date',
        'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',  
    ),
  );
  $data['diet_ingestion']['edit_ingestion'] = array(
     'field' => array(
       'title' => t('Edit'),
       'help' => t('UI edit link.'),
       'handler' => 'diet_handler_field_ingestion_link_edit',
     ),
   );
   $data['diet_ingestion']['energy'] = array(
     'title' => t('Energy'),
     'help' => t('Energy of the ingestion.'),
     'field' => array(
         'handler' => 'views_handler_field_numeric',
         'click sortable' => TRUE,
     ),
     'sort' => array(
       'handler' => 'views_handler_sort',  
     ),
   );
   
  
  // table diet_food_measures
  $data['diet_food_measures']['table']['group'] = t('Food measure');
  $data['diet_food_measures']['table']['join'] = array(
   'node' => array(
      'left_table' => 'diet_food',
      'left_field' => 'nid',
      'field'=> 'fid',
    ),
  );
  
  $data['diet_food_measures']['gr'] = array(
    'title' => t('Weight'),
    'help' => t('Weight of a measure'),
    'field' => array(
        'handler' => 'views_handler_field',
    ),
  );
  
  
  // table measure_images
  $data['measure_images']['table']['group'] = t('Food measure');
  $data['measure_images']['table']['join'] = array(
   'node' => array(
      'table' => 'node_images',
      'left_table' => 'diet_food_measures',
      'left_field' => 'nid',
      'field'=> 'nid',
    ),
  );
  $data['measure_images']['node_images_display'] = array(
    'real field'=> 'id',
    'title' => t('Images'),
    'help' => t('Image of a measure'),
    'field' => array(
        'handler' => 'node_images_query_handler_display',
    ),
  );
  
  // table diet_food_cart
  $data['diet_food_cart']['table']['group'] = t('Food cart');
  $data['diet_food_cart']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field'=> 'nid',
    ),
  );
  
  // uid
  $data['diet_food_cart']['uid'] = array(
    'title' => t('Uid'),
    'help' => t('The user ID'), // The help that appears on the UI,
    'field' => array(
      'handler' => 'views_handler_field_user',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'diet_handler_argument_food_cart_uid',
      'name field' => 'name', // display this field in the summary
    ),
    'filter' => array(
      'title' => t('Name'),
      'handler' => 'views_handler_filter_user_name',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  // date_in field
  $data['diet_food_cart']['date_in'] = array(
    'title' => t('Date first day'), // The item it appears as on the UI,
    'help' => t('The first day of the application.'), // The help that appears on the UI,
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  // date_out field
  $data['diet_food_cart']['date_out'] = array(
    'title' => t('Date last day'), // The item it appears as on the UI,
    'help' => t('The last day of the application.'), // The help that appears on the UI,
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  
  
  return $data;
}




//TODO put all views hooks in a separated file?

/**
 * Implementation of hook_views_handlers().
 */
function diet_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'diet') .'/views',
      ),
    'handlers' => array(
      'diet_handler_meal_energy' => array(
        'parent' => 'views_handler_field',
      ),
      'diet_handler_field_user_name' => array(
        'parent' => 'views_handler_field_user',
      ),
      'diet_filter_field_user_name' => array(
        'parent' => 'views_handler_filter_in_operator',
      ),
      'node_images_query_handler_display' => array(
        'parent' => 'views_handler_field',
      ),
      'diet_handler_field_ingestion_link_edit' => array(
        'parent' => 'views_handler_field_node_link',
      ),
      'diet_handler_argument_food_cart_uid' => array(
        'parent' => 'views_handler_argument_numeric',
      ),
      /*
      'diet_handler_field_node_link_clone' => array(
        'parent' => 'views_handler_field_node_link',
      ),
      */
    ),
  );
}