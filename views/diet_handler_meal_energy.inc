<?php
/**
 * Field handler to provide the total energy of a meal.
 */
class diet_handler_meal_energy extends views_handler_field {

  function construct() {
    parent::construct();
    $this->additional_fields['mid'] = array('table' => 'diet_meal', 'field' => 'nid');
  }

  function query() {
    $this->add_additional_fields();
    $this->field_alias = $this->aliases['mid'];
  }


  function pre_render($values) {
    foreach($values as $value) {
      $mid = $value->{$this->field_alias};
      $total_energy = 0;
      $result = db_query("SELECT energy FROM {diet_food_meal} WHERE mid = %d", $mid);
      while ($row = db_fetch_object($result)) {
        $total_energy += $row->energy; 
      }
      //var_dump($total_energy);
      $value->{$this->field_alias} = $total_energy;
      $values[] = $value;
    }
    
    
  }
  
}
