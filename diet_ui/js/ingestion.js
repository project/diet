

jQuery.fn.waitingJFrame = function() {
    // Overload this function in your code to place a waiting event
    // message, like :  $(this).html("<b>loading...</b>");
    var text = Drupal.t('Loading...');
    $(this).html("<b>"+ text +"</b>");  
}

    



$(document).ready(function() {
   $("input[id='edit-meal']").focus(function() {
     
     $("input[id='edit-meal']").val('');
     
   });
  
  
  
  $("input[id='edit-add-meal']").bind("click", function() {
    $("input[id='edit-meal']").val('');
    /*
    jQuery(document).find("div[id='diet-edit-ingestion-meal-measures']").each(function(i) {
        jQuery(this).loadJFrame(measures_src, undefined, true);
    });
    */
  }); 
  
   
  $("input[id='edit-add-user-meal']").bind("click", function() {
    $("input[id='edit-title']").val('');
  });  

   

  
  
  /*floating frames 
  $(window).scroll(function(){
    var width = $("#floating-top").width();
      if  ($(window).scrollTop() > $("#floating-top").offset({ scroll: false }).top){
        $("#floating-window").css("position", "fixed");
        $("#floating-window").css("top", "0");
        $("#floating-window").css("width", width);
      }
     if  ($(window).scrollTop() <= $("#floating-top").offset({ scroll: false }).top){
        $("#floating-window").css("position", "relative");
       $("#floating-window").css("top", $("#floating-top").offset);
      }
     });
  */
  //update frames when submiting the meal weight form
  $("form[id='diet-ui-meal-weight-form']").submit(function(){
    mid = $('form#diet-ui-meal-weight-form > input#edit-mid').val();
    jQuery($('#food-list')).loadJFrame('/diet_ui/meal/' + mid + '/food_list/edit');
  });
  
  //update frames when submiting the show meal form
  $("form[id='diet-ui-add-meal-subform']").submit(function(){
    jQuery($('#diet-edit-ingestion-meal-measures')).loadJFrame('/');
  });
  $("form[id='diet-ui-add-user-meal-subform']").submit(function(){
    jQuery($('#diet-edit-user-meal-create-form')).loadJFrame('/');
  });
  
  //close new food form lightbox frame on submit
  //$('form#diet-ui-new-food-form').submit(function() { Lightbox.end('forceClose'); return false; } );
  
  //new autocomplete field with autocomplete jquery plugin
  /*
  $("#edit-meal").autocomplete('/mealsearch.php', {
    extraParams: {uid : uid},
    minChars: 1,
    max: 1000,
  });
  */
  
});

/*
Drupal.behaviors.autosubmit = function () {
  $("input[id='edit-fname']").blur(function() {
    $("form[id='diet-ui-meal-food-add-form']").submit();
  });
};
*/