<?php

/**
 * @file diet-meal-details.tpl.php
 * Template for the meal details.
 */
?>


<h2><?php print $title; ?></h2>

<div class="meal-image"><?php print $image; ?></div>


<?php
if ($body) {
?>
<div class="meal-body grid-8 alpha"><?php print $body; ?></div>
<?php
}
?>
<div id="edit-meal-button">
  <a href="<?php print $edit_meal_url; ?>" target="diet-edit-ingestion-meal-details" ><?php print $edit_meal_link_text ?></a>
</div>
<?php
if ($edit_meal_url_2) {
?>
<div id="edit-meal-button-2">
  <a href="<?php print $edit_meal_url_2; ?>" target="diet-edit-ingestion-meal-details" ><?php print $edit_meal_link_text_2 ?></a>
</div>
<?php
}
?>
<h4><?php print t('Ingredients'); ?></h4>
<div class="food-list"><?php print $food_list; ?>
  
</div>

<div id="add-meal-button">
  <!--<a href="<?php print $add_meal_url; ?>" target="diet-edit-ingestion-meals">-->
  <a href="<?php print $add_meal_url; ?>"><?php print t('Add meal to the ingestion'); ?></a>
</div>