<?php

/**
 * @file Diet_ui.theme.inc
 *
 * theme functions for the diet_ui module
 *
 * © Copyright 2008 Robert Garrigos.
 * \author Robert Garrigos http://garrigos.cat
 */
 

 
/**
 * @ingroup diet_ui
 * @{
 */

/**
 * Preprocess the add ingestion page.
 */
function template_preprocess_diet_ui_edit_ingestion(&$vars) {
  $ingestion        = $vars['ingestion'];
  $vars['date']     = format_date($ingestion->date, 'custom', 'l, d/M/Y');
  $uid              = $vars['ingestion']->uid;
  $vars['type']     = $ingestion->type['name'];
  $vars['src_type'] = url("user/$uid/diet/ingestion/$ingestion->nid/meals");
  $vars['add_meal'] = drupal_get_form('diet_ui_add_meal_subform', $ingestion->nid);
    //$j = "javascript:$('#diet-edit-ingestion-meal-details').loadJFrame('" . url("diet_ui/ingestion/$ingestion->nid/meal/create") . "');";
    //$vars['create_user_meal'] = '<a href="'. $j . '">' . t('Create a new meal') . '</a>';
  $vars['create_user_meal'] = drupal_get_form('diet_ui_add_user_meal_subform', $ingestion->nid);
  $vars['ingestion_id'] = $ingestion->nid;
  $vars['food_cart'] = url("diet/food_cart/ingestion/$ingestion->nid");
  $vars['meal_details_src'] = url("diet_ui/ingestion/$ingestion->nid/meal/details");
  $vars['ingestion_info'] = theme('diet_ui_ingestion_info', $ingestion);
  
  
  drupal_add_js('var measures_src = '. drupal_to_js(url("diet_ui/measure")) .";", 'inline');
  drupal_add_js('var uid = '. drupal_to_js($uid) .";", 'inline');
  drupal_add_js('var diet_ui_dir = '. drupal_to_js(drupal_get_path('module', 'diet_ui')) .";", 'inline');

}

/**
 * Preprocess the ingestion info.
 */
function template_preprocess_diet_ui_ingestion_info(&$vars) {
  $ingestion        = $vars['ingestion'];
  $vars['update_info_form'] = drupal_get_form('diet_ui_update_info_form', $ingestion->nid);
  $vars['food_cart_ingestion_left'] = url("diet/food_cart/ingestion/$ingestion->nid/week/left");
  $vars['ingestion_nutrients_chart'] = url("diet/nutrients_chart/ingestion/$ingestion->nid");
  $vars['assistant'] = url("diet/assistant/$ingestion->nid");
  
  
}

/**
 * Preprocess the meals ingestion list.
 */
function template_preprocess_diet_ui_meals_ingestion(&$vars) {
  $ingestion        = $vars['ingestion'];
  $vars['meals'] = theme('diet_ui_ingestion_meal_list', $ingestion->get_meals($ingestion->nid), true);
  
  //TODO create the shopping food_cart content 
}

/**
 * Preprocess the meal details.
 */
function template_preprocess_diet_ui_meal_details(&$vars) {
  $meal = $vars['meal'];
  $user = $vars['user'];
  $vars['title'] = $meal->title;
  $vars['body'] = $meal->body;
  $ingestion_id = arg(2);
  
  //image variable
  $meal->node_images = node_images_load($meal, 'teaser');
  //var_dump($meal->node_images);
  $total_weight = _diet_calculate_meal_weight($meal);
  // look for the proper image to show according to the meal's weight  
  //foreach ($meal->node_images as $image) {
    $image = array_pop($meal->node_images);
    // we include the weight of the meal into the image filename as follows xxxxxxxx_150gr.jpg
    $fname = $image->filename;
    $w = explode('_', $fname);
    $w = explode('.', array_pop($w));
    $w = str_replace('gr', '',$w[0]);
   // var_dump($w);
   // var_dump($total_weight);
   // if (_diet_ui_compare_weight($w, $total_weight)) {
      $description = check_plain($image->description);
      $pattern = '<img src="%path" alt="%description"/>';
      $thumb = strtr($pattern, array('%path'=>file_create_url($image->thumbpath), '%description'=>$description));
      if ($info = @getimagesize($image->filepath)) {
        $width = $info[0] + 36;
        $height = $info[1] + 36;
      }
      //$output = $thumb;
      $path = file_create_url($image->filepath);
      $output = '<a href="/' . $image->filepath . '" title="'.$description.'" rel="lightbox" jframe="no">'.$thumb.'</a> ';
   // }
 // }
  $output .= '<script type="text/javascript">';
   $output .= 'Drupal.attachBehaviors();';
   $output .= '</script>';
  //$output = theme('node_images_view', $meal, 'teaser');
  $vars['image'] = $output;
  
  
  $vars['food_list'] = theme_diet_ui_meal_food_list($meal->nid);
  
  $vars['add_meal_url'] = _diet_ui_add_meal_url($ingestion_id, $meal->nid);
  
  // edit meal variables
  // this is an standard meal
  if ($meal->meal['uid'] == 0) { 
    $vars['edit_meal_url'] = url("diet_ui/ingestion/$ingestion_id/meal/$meal->nid/personalize");
    $vars['edit_meal_link_text'] = t('Edit and create a new meal.');
  }
  // this is a personalized meal
  else {//TODO comprovar que realment aquest sigui un plat personalitzat per l'usuari connectat
    $vars['edit_meal_url'] = url("diet_ui/ingestion/$ingestion_id/meal/$meal->nid/personalize");
    $vars['edit_meal_link_text'] = t('Edit and create a new meal.');
    $vars['edit_meal_url_2'] = url("diet_ui/ingestion/$ingestion_id/meal/$meal->nid/edit");
    $vars['edit_meal_link_text_2'] = t('Edit without creating a new meal.');
  }
  drupal_add_js('var measures_src = '. drupal_to_js(url("diet_ui/measure/meal/$meal->nid")) .";", 'inline');
  
}

/**
 * Preprocess the meal details edit page.
 */
function template_preprocess_diet_ui_meal_details_edit(&$vars) {
  //TODO this function needs to be adapted to the edit functionallity 
  // as well as its tpl file (diet-meal-details-edit.tpl.php)
  global $base_url;
  $meal = $vars['meal'];
  //var_dump($meal);
  $user = $vars['user'];
  $vars['title'] = diet_ui_inline_meal_title_link($meal->nid, $meal->title);
  $vars['log'] = diet_ui_inline_meal_log_link($meal->nid, $meal->log);
  $vars['body'] = $meal->body;
  $ingestion_id = arg(2);
  
  //image variable
  /* TODO what should we do with the image of a personalized meal????
  $meal->node_images = node_images_load($meal, 'teaser');
  $output = theme('node_images_view', $meal, 'teaser');
  $vars['image'] = $output;
  */
  
  $vars['food_list_url'] = url("diet_ui/meal/$meal->nid/food_list/edit");
  
  // add_food_button
  $j = "javascript:$('#diet-edit-ingestion-meal-measures').loadJFrame('".url("diet_ui/meal/$meal->nid/food/add")."');";
  //$vars['add_food_button'] = l(t('Add a food to this list of ingredients'), $j, $options);
  $vars['add_food_button'] = '<a href="'. $j . '">' . t('Add an ingredient.') . '</a>';
  
  
  // change_meal_weight_button
  $j = "javascript:$('#diet-edit-ingestion-meal-measures').loadJFrame('".url("diet_ui/meal/$meal->nid/weight/edit")."');";
  //$vars['add_food_button'] = l(t('Add a food to this list of ingredients'), $j, $options);
  $vars['change_meal_weight_button'] = '<a href="'. $j . '">' . t("Change meal's weight") . '</a>';
  
  $vars['add_meal_url'] = _diet_ui_add_meal_url($ingestion_id, $meal->nid);
  
  // edit meal variables
  // this is an standard meal
  if ($meal->meal['uid'] == 0) { 
    $vars['edit_meal_url'] = url("diet_ui/ingestion/$ingestion_id/meal/$meal->nid/personalize");
    $vars['edit_meal_link_text'] = t('Personalize this meal by creating a new one based on these ingredients.');
  }
  // this is a personalized meal
  else {//TODO comprovar que realment quest sigui un plat personalitzat per l'usuari connectat
    $vars['edit_meal_url'] = url("diet_ui/ingestion/$ingestion_id/meal/$meal->nid/personalize");
    $vars['edit_meal_link_text'] = t('Personalize this meal by creating a new one based on these ingredients.');
    $vars['edit_meal_url_2'] = url("diet_ui/ingestion/$ingestion_id/meal/$meal->nid/edit");
    $vars['edit_meal_link_text_2'] = t('Edit this personalized meal without creating a new one.');
  }
    
}

/**
 * prints an ingestion list for a given user.
 *
 * @param $result
 *   int. mysql result set.
 */
function theme_diet_ui_ingestion_list($result) {
  $rows = array();
  
  while ($i = db_fetch_object($result)) {
    $row = array();
    $uid = $i->iuid > 0 ? $i->iuid : $i->nuid;
    $row[] = l($i->title, "node/$i->nid");
    $row[] = l(t('Edit'), "user/$uid/diet/ingestion/$i->nid/edit");
    
    $rows[] = $row;
  }
  
  $header = array(t('Ingestion'), t('Actions'));
  $output = theme('table', $header, $rows);
  return $output;
}

/**
 * prints a table of actual meal list of an ingestion.
 *
 * @param $meal_list
 *   array. meal_list property of an ingestion object.
 * @param $edit
 *   boolean. whether the edit linkshould be shown (for the meal edit form) or not.
 * @return
 *   description of return value.
 */
function theme_diet_ui_ingestion_meal_list($meal_list, $edit = FALSE) {
  global $user;
  //var_dump($meal_list);
  $edit = ($edit || array_key_exists(variable_get('diet_admin_role', ''), $user->roles));
  
  $rows = array();
  $row = array();
  
  $header = array(
    t('Meal'), 
    array(
      'data' => t('Weight'), 'style' => 'text-align:center;',
    ),
    array(
      'data' => t('Energy'), 'style' => 'text-align:center;',
    ),
  );
  if ($edit) {
    $header[] = array(
      'data' => t('Action'), 'style' => 'text-align:center;',
    );
  }

  foreach ($meal_list as $key => $meal) {
    $meal['mname'] = db_result(db_query("SELECT n.title FROM {node} n WHERE n.nid = %d AND type = 'meal'", $meal['mid']));
    $meal['mname'] = tt("meal:node_" . $meal['mid'], $meal['mname']);
    $link = l($meal['mname'], 'diet/meal/' . $meal['mid'], array('attributes' => array('jframe' => 'no', 'rel' => 'lightframe[|width:400px; height:550px;]')));
        $meal['mname'] = $link;
    $meal['energy'] = _diet_get_meal_energy($meal['mid']);
    $meal['weight'] = _diet_calculate_meal_weight($meal['mid']);
    if ($edit) {
      $iid = $meal['iid'];
      $mid = $meal['mid'];
      //jframes to reload when deleting a meal from an ingestion
      $w = "$('#diet-edit-ingestion-meals').loadJFrame('".url("diet/ingestion/$iid/meal/$mid/delete?destination=/user/$user->uid/diet/ingestion/$iid/meals")."');";
      //$w .= "$('#diet-edit-ingestion-meal-suggestions').loadJFrame('".url("diet/assistant/$iid")."');";
      $w .= "$('#diet-edit-ingestion-meal-measures').loadJFrame('".url("diet_ui/measure")."');";
      $w .= "$('#diet-edit-ingestion-meal-details').loadJFrame('".url("diet_ui/ingestion/$iid/meal/details")."');";
      $w .= "$('#diet-edit-ingestion-food-cart').loadJFrame('".url("diet/food_cart/ingestion/$iid")."');";
     // $w .= "$('#diet-edit-ingestion-left-food-cart').loadJFrame('".url("diet/food_cart/ingestion/$iid/week/left")."');";
      
      $delete = "<a href=\"javascript:$w\">" . t('Delete') . "</a>";

      $rows[] = array(
        $meal['mname'],
        array(
          'data' => $meal['weight'], 'style' => 'text-align:center;',
        ),
        array(
          'data' => $meal['energy'], 'style' => 'text-align:center;',
        ),
        array(
          'data' => $delete, 'style' => 'text-align:center;',
        ), 
        
      );
    }
    else {
      //TODO apply the text align center to this row
      $rows[] = array($meal['mname'], $meal['energy']);
    }
    $total_energy += $meal['energy'];
    $total_weight += $meal['weight'];
  }
  $rows[] = array(
    array(
      'data' => t('Total') . ':', 'style' => 'font-weight:bold; text-align:right',
    ),
    array(
      'data' => $total_weight . ' Gr.', 'style' => 'font-weight:bold; text-align:center;',
    ),
    
    array(
      'data' => $total_energy . ' Kcal.', 'style' => 'font-weight:bold; text-align:center;',
    ),
    '',
  );
  
  $output = theme('table', $header, $rows);
  return $output;
}

/**
 * prints a table of actual foods list of a meal in the ingestion ui.
 *
 * @param $mid
 *   int. meal id
 */
function theme_diet_ui_meal_food_list($mid, $edit = FALSE) {
  
  $output = '';
  $meal = node_load($mid);
  $rows = array();
  foreach ($meal->food_list as $f) {
    $food = node_load($f['fid']);
    $continue = TRUE;
    //check whether its weight is more o less a measure
    foreach ($food->food_measures_list as $m) {
      if (_diet_ui_compare_weight($m, $f) && $continue) {
        $measure = node_load($m['nid']);
        $term = array_shift($measure->taxonomy);
        //we omit the decimal if 0
        $qty = explode('.', $m['qty']);
        $qty = $qty[1] == '0' ? $qty[0] : implode('.', $qty);
        $link = $edit ? 'diet_ui/measure/'. $m['nid'] . '/meal/' . $mid : 'diet_ui/measure/'. $m['nid'];
        $unit = l($qty . ' ' . $term->name, $link, array('attributes' => array('target' => 'diet-edit-ingestion-meal-measures')));
        $continue = FALSE;
      }
    }
    $options = array(
      'attributes' => array(
        'target' => 'food-list',
      ),
      'query' => "destination=diet_ui/meal/$meal->nid/food_list/edit",
    );
    
    $delete = l(t('Delete'), "diet/meal/$meal->nid/food/$food->nid/delete/" . $f['gr'], $options);
    unset($options);
    $weight_link = $edit ? 'diet_ui/food_weight/'. $f['fid'] . '/meal/' . $mid : 'diet_ui/measure/'. $m['nid'];
    $weight = $edit ? l($f['gr'], $weight_link, array('attributes' => array('target' => 'diet-edit-ingestion-meal-measures'))) : $f['gr'];
    $a_row = array(
      array(
        'data' => $food->title, 'style' => 'width:35%;'
      ),
      // not using this for now
      //$unit,
      array(
        'data' => $weight, 'style' => 'text-align:center;'
      ),
      array(
        'data' => $f['energy'], 'style' => 'text-align:center;'
      ),
    );
    if ($edit) {
      $a_row[] = $delete;
    }
    $rows[] = $a_row;
    unset ($unit);
    $total_weight += $f['gr'];
    $total_energy += $f['energy'];
  }
  $last_row = array(
    array(
      //'data' => t('Totals') . ':', 'colspan' => 2, 'style' => 'font-weight:bold; text-align:right;'
      // not using unit column for now
      'data' => t('Totals') . ':', 'style' => 'font-weight:bold; text-align:right;'
    ),
    array(
      'data' => $total_weight . ' gr.', 'style' => 'font-weight:bold; text-align:center;'
    ),
    array(
      'data' => $total_energy . ' Kcal.', 'style' => 'font-weight:bold; text-align:center;'
    ),
  );
  if ($edit) {
    $last_row[] = '';
  }
  $rows[] = $last_row;
  //$header = array(t('Ingredient'), t('Unit'), t('Weight'), t('Energy'));
  // not using the unit column for now
  $header = array(t('Ingredient'), t('Weight'), t('Energy'));
  if ($edit) {
    $header[] = t('Action');
  }
  
  $output .= theme('table', $header, $rows);
  
  $error = theme_status_messages('error');
  
  return $error . $output;
}


/**
 * prints a javascript href link to update all frames when adding a new meal to an ingestion.
 *
 * @param $iid  
 *   int. ingestion id.
 * @param $mid
 *   int. meal id.
 * @return
 *   string.
 */
function _diet_ui_add_meal_url($iid, $mid) {
  $f = "$('#diet-edit-ingestion-meals').loadJFrame('". url("diet_ui/ingestion/$iid/meal/$mid/add")."');";
  //$f .= "$('#diet-edit-ingestion-meal-suggestions').loadJFrame('".url("diet/assistant/$iid")."');";
  $f .= "$('#diet-edit-ingestion-meal-measures').loadJFrame('".url("diet_ui/measure")."');";
  $f .= "$('#diet-edit-ingestion-meal-details').loadJFrame('".url("diet_ui/ingestion/$iid/meal/details")."');";
  $f .= "$('#diet-edit-ingestion-food-cart').loadJFrame('".url("diet/food_cart/ingestion/$iid")."');";
  //$f .= "$('#diet-edit-ingestion-left-food-cart').loadJFrame('".url("diet/food_cart/ingestion/$iid/week/left")."');";
  
  $link = "javascript:$f";
  return $link;
}

/**
 * theme function to print a measure image, either to add or edit a food 
 * of a given meal.
 *
 * @param $image
 *   object. image as given by node_images_load().
 * @param $action
 *   string. add or edit action.
 * @param $measure
 *   object. measure node
 * @param $mid
 *   int. meal node id.
 */
function theme_diet_ui_measure_image($image, $action, $measure, $mid) {
  $output = '';
  $description = check_plain($image->description);
  $pattern = '<img src="%path" alt="%description" />';
  $thumb = strtr($pattern, array('%path'=>file_create_url($image->thumbpath), '%description'=>$description));
  $output .= '<div class="measure-image">';
  $w = '';
  $w .= "$('#diet-edit-ingestion-meal-measures').loadJFrame('".url("diet_ui/inline")."');";
  $w .= "$('#food-list').loadJFrame('" . url("diet_ui/meal/$mid/food_list/" . $action .  "_measure/$measure->nid") . "');";
  $output .= "<a href=\"javascript:$w\">$thumb</a>";
  $output .= t('Weight: @weight gr.', array('@weight' => $measure->gr));
  $output .= '</div>';
  return $output;
}

/**
 * @} End of defgroup diet_ui_theme.
 */
