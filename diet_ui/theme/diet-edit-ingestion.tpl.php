<?php

/**
 * @file diet-add-ingestion.tpl.php
 * Template for the add ingestion page.
 */
?>

<div id="diet-edit-ingestion" class="grid-16 alpha">
  <div id="diet-ui-left-col" class="grid-12 alpha">
    <div id="diet-edit-ingestion-main" class="grid-12 alpha">
        <h3>
          <?php print $date; ?>
        </h3>
        <div id="diet-edit-ingestion-meals" class="diet-edit-ingestion-type">
          <h2>
            <?php print $type; ?>
          </h2>
          <div id="diet-edit-ingestion-meal-list" src="<?php print $src_type; ?>"></div>

        </div>
        <!--<div id="diet-edit-ingestion-food-cart" class="grid-7 alpha" src="<?php print $food_cart; ?>"></div>-->
        
    </div>
    <div class="clear-block"></div>
    <div id="diet-ingestion-meal-wrapper" class="grid-12 alpha">
      <div id="diet-ui-left-col" class="grid-12 alpha">
        <div id="diet-edit-ingestion-add-forms" class="grid-6 alpha">
          <div class="inside">
            <h3>
              <?php print t('Meal search') ?>
            </h3>
            <div id="diet-edit-ingestion-add-forms-in">
              <div id="diet-edit-ingestion-add-meal-newform" src="#">
                <?php print $add_meal; ?>
              </div>
            </div>
          </div>
        </div>
        <div id="diet-edit-ingestion-add-user-meal-form" class="grid-6 omega">
          <div class="inside">
            <h3>
              <?php print t('Create a new meal') ?>
            </h3>
            
            <div id="diet-edit-user-meal-create-form"  src="#">
              <?php print $create_user_meal; ?>
            </div>
          </div>
        </div>
        <div class="clear-block"></div>
        <div id="diet-ui-left-left-col" class="grid-7 alpha">
          <div id="diet-edit-ingestion-meal-details" src="<?php print $meal_details_src; ?>"></div>
        </div>
        <div id="diet-ui-left-right-col" class="grid-5 omega">
          <div id="diet-edit-ingestion-meal-measures" src="#"></div>
        </div>
        </div>
      </div>

    </div>

  
  
  <div id="diet-ui-right-col" class="grid-4 omega">
    <div id="diet-ui-ingestion-info" src="#">
    <?php print $ingestion_info; ?>
    </div>
  </div>

</div>