<?php

/**
 * @file diet-meal-details-edit.tpl.php
 * Template for the meal details edit page.
 */
?>
<!--
<div class="messages">
  <div class="status">
  <?php print t('Click on the elements you want to edit. When you finish click on the upper button to add this meal to the ingestion.') ?>
  </div>
</div>
-->


<?php print $title; ?>
<?php print $log; ?>

<div class="meal-image"><?php print $image; ?></div>

<?php
if ($body) {
?>
<div class="meal-body grid-8 alpha"><?php print $body; ?></div>
<?php
}
?>
<div id="change-meal-weight-button">
  <?php print $change_meal_weight_button; ?>
</div>
<div id="add-food-button">
  <?php print $add_food_button; ?>
</div>
<h4><?php print t('Ingredients'); ?></h4>
<div id="food-list" class="food-list" src="<?php print $food_list_url; ?>"></div>



<div id="add-meal-button" class='edit-meal'>
  <a href="<?php print $add_meal_url; ?>"><?php print t('Add meal to the ingestion'); ?></a>
</div>