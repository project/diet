<?php

/**
 * @file diet-ingestion-info.tpl.php
 * Template for the info of the ingestion.
 */
?>
<div id="diet-edit-ingestion-left-food-cart" src="<?php print $food_cart_ingestion_left; ?>"></div>
<!--<?php print $update_info_form; ?>-->
<div id="diet-edit-ingestion-nutrients-cart" src="<?php print $ingestion_nutrients_chart; ?>"></div>
<div id="diet-edit-ingestion-meal-suggestions" src="<?php print $assistant; ?>"></div>
