<?php

/**
 * @file diet-food-field.tpl.php
 * Template for a food field.
 */
?>
<div class="food-field">
  <span class="food-field-title"><?php print $field ?>: </span><span class="food-field-value"><?php print $value ?></span>
</div>

