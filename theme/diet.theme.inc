<?php

/**
 * @defgroup diet_theme Diet module theme functions.
 * @{
 * Group of main theme functions for the Diet module.
 * 
 */

/**
 * prints a table of actual foods list of a meal.
 *
 * @param $food_list
 *   array. food_list property of a meal object with the following structure
 *   - food_list = array {
 *    -  array {
 *     -  fid
 *     -  mid
 *     -  gr
 *     -  energy
 *     }
 *   }
 * @param $edit
 *   boolean. whether the edit linkshould be shown (for the meal edit form) or not.
 * @param $
 *   description of parameter.
 * @return
 *   description of return value.
 */
function theme_diet_meal_food_list($food_list, $edit = NULL) {
  global $user;
  $edit = ($edit || array_key_exists(variable_get('diet_admin_role', ''), $user->roles));
  $rows = array();
  $row = array();
  if ($edit) {
    $header = array(t('Food'), t('Weight (g.)'), t('Energy'), t('Action'));
    $colspan = 4;
  }
  else {
    $header = array(t('Food'), t('Weight (g.)'), t('Energy'));
    $colspan = 3;
  }
  foreach ($food_list as $key => $food) {
    //TODO make this query food ddbb independent
    $food['fname'] = db_result(db_query("SELECT n.title FROM {node} n WHERE n.nid = %d", $food['fid']));
    $food['fname'] = tt("food:node_" . $food['fid'], $food['fname']);
    if ($edit) {
    //  $delete = l(t('delete'), 'diet/meal/'. $food['mid'] . '/food/' . $food['fid'] . '/delete', array('query' => array('destination' => 'diet/edit/meal/' . $food['mid'])));
      $delete = l(t('delete'), 'diet/meal/'. $food['mid'] . '/food/' . $food['fid'] . '/delete', array('query' => array('destination' => diet_get_destination())));
      $rows[] = array($food['fname'], $food['gr'], $food['energy'], $delete);
    }
    else {
      $rows[] = array($food['fname'], $food['gr'], $food['energy']);
    }
    $total_energy += $food['energy'];
  }
  $rows[] = array(array('data' => t('Total energy: @energy', array('@energy' => $total_energy)), 'colspan' => $colspan, 'style' => 'font-weight:bold;'));
  
  $output = theme('table', $header, $rows);
  return $output;
}

/**
 * prints a vocabulary form item for a meal.
 *
 * @param $form
 *   form elements.
 */
function theme_diet_meal_vocabularies($form) {
  //var_dump($form);
  $rows = array();
	$row = array();
	foreach($form as $key => $element) {
	  if (is_numeric($key)) {
	    $row[] = drupal_render($form[$key]);
	  }
	}
//	$row[] = drupal_render($form['remove']);
	$rows[] = $row;
	$output = theme('table', array(), $rows);
	$output .= drupal_render($form);
	return $output;
}

/**
 * Theme the add new subwidget subform as a single row.
 */
function theme_diet_new_food_element(&$form) {
  $rows = array();
	$row = array();
	$row[] = drupal_render($form['fname']);
	$row[] = drupal_render($form['gr']);
//	$row[] = drupal_render($form['remove']);
	$rows[] = $row;
	$output = theme('table', array(), $rows);
	$output .= drupal_render($form);
	return $output;
}

/**
 * prints admin links in meal node view.
 *
 * @param $node
 *   objcet. node.
 * @return
 *   string. html.
 */
function theme_diet_meal_admin_links($node) {
  $dest = drupal_get_destination();
  $links = array();
  $output = '';
  $output .= '<div class="meal_admin_links">';
  $links[] = l(t('Add food'), "diet/meal/$node->nid/food/add", array('query' => $dest));
  $output .= implode(' ', $links);
  $output .= '</div>';
  return $output;
}

/**
 * prints admin links in food node view.
 *
 * @param $node
 *   objcet. node.
 * @return
 *   string. html.
 */
function theme_diet_food_admin_links($node) {
  //$dest = drupal_get_destination();
  $links = array();
  $output = '';
  $output .= '<div class="food_admin_links">';
  $links[] = l(t('Add a measure'), "diet/food/$node->nid/measure/add");
  $output .= implode(' ', $links);
  $output .= '</div>';
  return $output;
}

/**
 * prints a table of actual food measures list of a food.
 *
 * @param $food_measures_list
 *   array. food_measures_list property of a food object.
 * @param $edit
 *   boolean. whether the edit linkshould be shown (for the meal edit form) or not.
 * @param $
 *   description of parameter.
 * @return
 *   description of return value.
 */
function theme_diet_food_measures_list($food_measures_list, $edit = NULL) {
  global $user;
  $edit = ($edit || array_key_exists(variable_get('diet_admin_role', ''), $user->roles));
  $rows = array();
  $row = array();
  if ($edit) {  
    $header = array(t('Image'), t('Qty'), t('Measure'), t('Weight (gr.)'), t('Action'));
    $colspan = 5;
  }
  else {
    $header = array(t('Image'), t('Qty'), t('Measure'), t('Weight (gr.)'));
    $colspan = 4;
  }
  foreach ($food_measures_list as $key => $measure) {
    //TODO adapt this to a food measure 
    $measure['measure'] = db_result(db_query("SELECT td.name FROM {term_data} td LEFT JOIN {term_node} tn ON tn.tid = td.tid WHERE tn.nid = %d", $measure['nid']));
    
    // TODO simplify this
    $mnode = node_load($measure['nid']);
    $mnode->node_images = node_images_load($mnode, $teaser, $page);
    $image = theme('node_images_view', $mnode, $teaser, $page);
    //$image = '<div class="node_images">'.$output.'</div>';
    
    if ($edit) {
      $delete = l(t('delete'), 'diet/food/'. $measure['fid'] . '/measure/' . $measure['nid'] . '/delete', array('query' => array('destination' => 'node/' . $measure['fid'])));
      $edit_link = l(t('edit'), 'node/' . $measure['nid'] . '/edit', array('query' => array('destination' => 'node/' . $measure['fid'])));
      
      $rows[] = array($image, $measure['qty'], $measure['measure'], $measure['gr'], $delete . ' | ' . $edit_link);
    }
    else {
      $rows[] = array($image, $measure['qty'], $measure['measure'], $measure['gr']);
    }
  }
  
  $output = theme('table', $header, $rows);
  return $output;
}

/**
 * Preprocess the add ingestion page.
 */
function template_preprocess_diet_edit_ingestion(&$vars) {
  //var_dump($vars['ingestion']);
  diet_add_js('ajax');
  //drupal_add_js(drupal_get_path('module', 'date_popup'), '/date_popup.js');
  $ingestion        = $vars['ingestion'];
  $vars['date']     = format_date(strtotime($ingestion->date), 'custom', 'l, d/M/Y');
  $uid              = $vars['ingestion']->uid;
  $vars['type']     = $ingestion->type;
  $vars['src_type'] = "/user/$uid/diet/ingestion/type";
}

/**
 * Preprocess the fodd nutrients list of a food node view.
 */
function template_preprocess_diet_food_nutrients(&$vars) {
  
  foreach ($vars['food'] as $field => $value) {
       $nutrients .= theme('diet_food_field', $field, $value);
   }
   $render_array = array(
     '#type' => 'fieldset',
     '#title' => t('Nutrients'),
     '#collapsible' => true,
     '#collapsed' => true,
   );
   $render_array['text_contents'] = array(
     '#type' => 'markup',
     '#value' => '<div>' . $nutrients . '</div>',
   );
   $vars['nutrients'] = drupal_render($render_array);
}

/**
 * prints a fieldset form element containing the food types as a table.
 *
 */
function theme_diet_food_units_table($form) {
  $rows = array();
  foreach ($form as $key => $value) {
    if (is_numeric($key)) {
      $title = $value['#title'];
      unset($form[$key]['#title']);
      $rows[] = array($title, drupal_render($form[$key]));
    }
  }
  $header = array(t('Type'), t('Value'));
  $output = theme('table', $header, $rows);
  $output .= drupal_render($form);
  return $output;
}

/**
 * prints a list of food units for a food_cart view.
 *
 * @param $food_units
 *   array.
 * @return
 *   string. html.
 */
function theme_diet_food_units($food_units) {
  $rows = array();
  $output = '';
  
  foreach ($food_units as $tid => $qty) {
    $bar1 = '';
    $bar2 = '';
    $term = taxonomy_get_term($tid);
    $color = taxonomy_color_get_term_color($tid);

    $bar1 .= '<div class="negative-food-unit-bar">';
    if ($qty < 0) {
      for ($i=-1; $i >= $qty; $i--) { 
        $bar1.= '<div class="negative-food-unit" style="background:red;" ></div>';
      }
      // add any partial unit block
      if (floor(abs($qty)) < abs($qty)) {
        $width = (abs($qty) - floor(abs($qty))) * 10;
        //dpm($width);
        $bar1.= '<div class="negative-food-unit-decimal" style="width:' . $width . '%;background:red;float:right;height:20px;" ></div>';
      }
    }
    $bar1 .= '</div>';
    $bar2 .= '<div class="food-unit-bar">';
    if ($qty > 0) {
      for ($i=1; $i <= $qty; $i++) { 
        $bar2.= '<div class="positive-food-unit" style="background:'. $color .';" ></div>';
      }
      // add any partial unit block
      if (floor($qty) < $qty) {
        $width = ($qty - floor($qty)) * 5;
        $bar2.= '<div class="positive-food-unit-decimal" style="width:' . $width . '%;background:'. $color .';float:left;height:20px;" ></div>';
      }
    }
    $bar2 .= '</div>';
    $rows[$term->name] = array(
      array('data' => $term->name, 'class' => 'food-type'), 
      array('data' => $bar1, 'class' => 'negative-food-unit'), 
      array('data' => $bar2, 'class' => 'positive-food-unit')
      );
  }
  //TODO should we save term weight data and use that to sort the terms?
  sort($rows);
  $header = array(t('Type'), array('data' => t('Units'), 'colspan' => 2, 'style' => 'text-align:center;'));
  $output .= theme('table', $header, $rows);
  return $output;
}

/**
 * prints a list of food units for a food_cart view.
 * small view
 *
 * @param $food_units
 *   array.
 * @return
 *   string. html.
 */
function theme_diet_food_units_small($food_units) {
  $rows = array();
  $output = '';
  
  foreach ($food_units as $tid => $qty) {
    $bar1 = '';
    $bar2 = '';
    $term = taxonomy_get_term($tid);
    $color = taxonomy_color_get_term_color($tid);

    $bar1 .= '<div class="negative-food-unit-bar-small">';
    if ($qty < 0) {
      for ($i=-1; $i >= $qty; $i--) { 
        $bar1.= '<div class="negative-food-unit" style="background:red;height:10px;" ></div>';
      }
      // add any partial unit block
      if (floor(abs($qty)) < abs($qty)) {
        $width = (abs($qty) - floor(abs($qty))) * 10;
        $bar1.= '<div class="negative-food-unit-decimal" style="width:' . $width . '%;background:red;float:right;height:10px;" ></div>';
      }
    }
    else {
      $bar1 .= '&nbsp';
    }
    $bar1 .= '</div>';
    $bar2 .= '<div class="food-unit-bar-small">';
    if ($qty > 0) {
      for ($i=1; $i <= $qty; $i++) { 
        $bar2.= '<div class="positive-food-unit" style="background:'. $color .';" ></div>';
      }
      // add any partial unit block
      if (floor($qty) < $qty) {
        $width = ($qty - floor($qty)) * 5;
        $bar2.= '<div class="positive-food-unit-decimal" style="width:' . $width . '%;background:'. $color .';float:left;" ></div>';
      }
    }
    else {
      $bar2 .= '&nbsp';
    }
    $bar2 .= '</div>';
    $rows[$term->name] = array(
      array('data' => '<div class="food-unit-term">' . $term->name . '</div>' . '<div class="food-unit-wrapper">' . $bar1 . $bar2 . '</div>', 'style' => 'text-align:center;'), 
      );
  }
  //TODO should we save term weight data and use that to sort the terms?
  sort($rows);
  //$header = array(array('data' => t('Units'), 'style' => 'text-align:center;'));
  $attributes = array('class' => 'food-cart-small');
  $output .= theme('table', array(), $rows, $attributes);
  return $output;
}

/**
 * prints a list of food units for a food_cart view.
 * small view + wide
 *
 * @param $food_units
 *   array.
 * @return
 *   string. html.
 */
function theme_diet_food_units_small_wide($food_units) {
  $rows = array();
  $output = '';
  
  foreach ($food_units as $tid => $qty) {
    $bar1 = '';
    $bar2 = '';
    $term = taxonomy_get_term($tid);
    $color = taxonomy_color_get_term_color($tid);

    $bar1 .= '<div class="negative-food-unit-bar-small-wide">';
    if ($qty < 0) {
      for ($i=-1; $i >= $qty; $i--) { 
        $bar1.= '<div class="negative-food-unit" style="background:red;height:10px;" ></div>';
      }
      // add any partial unit block
      if (floor(abs($qty)) < abs($qty)) {
        $width = (abs($qty) - floor(abs($qty))) * 10;
        $bar1.= '<div class="negative-food-unit-decimal" style="width:' . $width . '%;background:red;float:right;height:10px;" ></div>';
      }
    }
    else {
      $bar1 .= '&nbsp';
    }
    $bar1 .= '</div>';
    $bar2 .= '<div class="food-unit-bar-small-wide">';
    if ($qty > 0) {
      for ($i=1; $i <= $qty; $i++) { 
        $bar2.= '<div class="positive-food-unit" style="background:'. $color .';" ></div>';
      }
      // add any partial unit block
      if (floor($qty) < $qty) {
        $width = ($qty - floor($qty)) * 5;
        $bar2.= '<div class="positive-food-unit-decimal" style="width:' . $width . '%;background:'. $color .';float:left;" ></div>';
      }
    }
    else {
      $bar2 .= '&nbsp';
    }
    $bar2 .= '</div>';
    $rows[$term->name] = array(
      //array('data' => '<div class="food-unit-term">' . $term->name . '</div>' . '<div class="food-unit-wrapper">' . $bar1 . $bar2 . '</div>', 'style' => 'text-align:center;'),
      array('data' => $term->name, 'class' => 'food-type'), 
      array('data' => $bar1, 'class' => 'negative-food-unit-small-wide'), 
      array('data' => $bar2, 'class' => 'positive-food-unit-small-wide'),
       
      );
  }
  //TODO should we save term weight data and use that to sort the terms?
  sort($rows);
  //$header = array(array('data' => t('Units'), 'style' => 'text-align:center;'));
  $attributes = array('class' => 'food-cart-small-wide');
  $output .= theme('table', $header, $rows, $attributes);
  
  return $output;
}



/**
 * print main data of an ingestion.
 *
 * @param $ingestion
 *   array.
 * @return
 *   string. html.
 */
function theme_diet_ingestion_data($ingestion) {
  $output = '';
  $output .= '<div class="ingestion_data">';
  $output .= '<div class="ingestion_user"><span class="ingestion_data_label">' . t('User') . ':</span> ' . $ingestion['user_name'] . '</div>';
  $output .= '</div>';
  return $output;
}

/**
 * prints admin links in ingestion node view.
 *
 * @param $node
 *   objcet. node.
 * @return
 *   string. html.
 */
function theme_diet_ingestion_admin_links($node) {
  //$dest = drupal_get_destination();
  $links = array();
  $output = '';
  $output .= '<div class="ingestion_admin_links">';
  $links[] = l(t('Add a meal'), "diet/ingestion/$node->nid/meal/add");
  $output .= implode(' ', $links);
  $output .= '</div>';
  return $output;
}

/**
 * Theme the add new subwidget subform as a single row.
 */
function theme_diet_new_meal_element(&$form) {
  $rows = array();
	$row = array();
	$form['m']['#disabled'] = TRUE;
	$row[] = drupal_render($form['mname']);
	$row[] = drupal_render($form['delete_meal']);
	$rows[] = $row;
	$output = theme('table', array(), $rows);
	$output .= drupal_render($form);
	return $output;
}

/**
 * print a meal element in a meal list of an ingestion.
 *
 */
function theme_diet_meal_element_text($element) {
  $size = empty($element['#size']) ? '' : ' size="'. $element['#size'] .'"';
  $maxlength = empty($element['#maxlength']) ? '' : ' maxlength="'. $element['#maxlength'] .'"';
  $class = array('form-text');
  $extra = '';
  $output = '';

  if ($element['#autocomplete_path'] && menu_valid_path(array('link_path' => $element['#autocomplete_path']))) {
    drupal_add_js('misc/autocomplete.js');
    $class[] = 'form-autocomplete';
    $extra =  '<input class="autocomplete" type="hidden" id="'. $element['#id'] .'-autocomplete" value="'. check_url(url($element['#autocomplete_path'], array('absolute' => TRUE))) .'" disabled="disabled" />';
  }
  _form_set_class($element, $class);

  if (isset($element['#field_prefix'])) {
    $output .= '<span class="field-prefix">'. $element['#field_prefix'] .'</span> ';
  }

  //$output .= check_plain($element['#value']);
  $output .= '<input type="text"'. $maxlength .' name="'. $element['#name'] .'" id="'. $element['#id'] .'"'. $size .' value="'. check_plain($element['#value']) .'"'. drupal_attributes($element['#attributes']) .' />';

  //$output .= '<input type="text"'. $maxlength .' name="'. $element['#name'] .'" id="'. $element['#id'] .'"'. $size .' value="'. check_plain($element['#value']) .'"'. drupal_attributes($element['#attributes']) .'" disabled="disabled" />';
  

  if (isset($element['#field_suffix'])) {
    $output .= ' <span class="field-suffix">'. $element['#field_suffix'] .'</span>';
  }

  return theme('form_element', $element, $output) . $extra;
}

/**
 * prints a table of actual meal list of an ingestion.
 *
 * @param $meal_list
 *   array. meal_list property of an ingestion object.
 * @param $edit
 *   boolean. whether the edit linkshould be shown (for the meal edit form) or not.
 * @param $dest
 *   array. as the query argument for function l() .
 * @return
 *   description of return value.
 */
function theme_diet_ingestion_meal_list($meal_list, $edit = NULL) {
  global $user;
  //var_dump($meal_list);
  $edit = ($edit || array_key_exists(variable_get('diet_admin_role', ''), $user->roles));
  $rows = array();
  $row = array();
  if ($edit) {
    $header = array(t('Meal'), t('Energy'), t('Action'));
    $colspan = 3;
  }
  else {
    $header = array(t('Meal'), t('Energy'));
    $colspan = 2;
  }
  foreach ($meal_list as $key => $meal) {
    
    //TODO make this query food ddbb independent
    $meal['mname'] = db_result(db_query("SELECT n.title FROM {node} n WHERE n.nid = %d AND type = 'meal'", $meal['mid']));
    $meal['mname'] = tt("meal:node_" . $meal['mid'], $meal['mname']);
    $meal['energy'] = _diet_get_meal_energy($meal['mid']);
    if ($edit) {
      $delete = l(t('delete'), 'diet/ingestion/'. $meal['iid'] . '/meal/' . $meal['mid'] . '/delete', array('query' => array('destination' => diet_get_destination())));
      $rows[] = array($meal['mname'], $meal['energy'], $delete);
    }
    else {
      $rows[] = array($meal['mname'], $meal['energy']);
    }
    $total_energy += $meal['energy'];
  }
  $rows[] = array(array('data' => t('Total energy: @energy', array('@energy' => $total_energy)), 'colspan' => $colspan, 'style' => 'font-weight:bold;'));
  
  $output = theme('table', $header, $rows);
  return $output;
}

/**
 * prints a meal nutrients chart.
 *
 * @param $food_list
 *   array. list of food elements of the meal.
 */
function theme_diet_meal_nutrients_chart($food_list) {
  $output = diet_meal_generate_nutrients_chart($food_list);
  return '<div id="nutrients-chart">'. $output .'</div>';
}

/**
 * generates the meal nutrients chart.
 *
 * @param $food_list
 *   description of parameter.
 */
function diet_meal_generate_nutrients_chart($food_list) {
  $protein = 0;
  $lipids = 0;
  $carbohydrats = 0;
  foreach ($food_list as $food) {
    $proteins += _diet_get_food_nutrient($food['fid'], $food['gr'] * 4);
    $lipids += _diet_get_food_nutrient($food['fid'], $food['gr'] * 9, 'lipids');
    $carbohydrats += _diet_get_food_nutrient($food['fid'], $food['gr'] * 4, 'carbohydrats');
    
  }  
  $totals = $proteins + $lipids + $carbohydrats;
  
  $chart = array(
    '#type' => 'pie2D',
    '#title' => t('Nutrients'),
    array(
      array('#value' => $proteins, '#label' => t('Proteins') .': '. round($proteins / $totals * 100) . '%'),
      array('#value' => $lipids, '#label' => t('Lipids') .': '. round($lipids / $totals * 100) . '%'),
      array('#value' => $carbohydrats, '#label' => t('Carbohydrats') .': '. round($carbohydrats / $totals * 100) . '%'),
      ),
    );

  return charts_chart($chart);

}

/**
 * shows a list of food carts for a given user.
 *
 * @param $uid
 *   int. user id.
 * @return
 *   string. html.
 */
function theme_diet_food_carts_list($uid) {
  $output = '';
  $result = db_query("SELECT * FROM {diet_food_cart} WHERE uid = %d ORDER BY date_in DESC", $uid);
  while ($fc = db_fetch_object($result)) {
    $result2 = db_query("SELECT DISTINCT(week) FROM {diet_food_cart_week} WHERE fcid = %d ORDER BY week DESC", $fc->nid);
    $weeks = array();
    while ($fcw = db_fetch_object($result2)) {
      $range = _diet_week_range($fc->date_in + (604800 * ($fcw->week - 1)));
      $weeks[$fcw->week] = $range;
    }
    $fcl[] = array(
      'fcid' => $fc->nid,
      'date_in' => $fc->date_in,
      'date_out' => $fc->date_out,
      'weeks' => $weeks,
    );
  }
  //dpm($fcl);
  $rows = array();
  foreach ($fcl as $fc) {
    $date_out = $fc['date_out'] == 0 ? '' : date('d/m/Y', $fc['date_out']);
    $title = db_result(db_query("SELECT title FROM {node} WHERE nid = %d", $fc['fcid']));
    //food cart row
    $rows[] = array(
      $title, 
      date('d/m/Y', $fc['date_in']), 
      $date_out,
    );
    $rows2 = array();
    foreach ($fc['weeks'] as $w => $week) {
      // food cart instance row
      $rows2[] = array(
        $w, 
        date('d/m/Y', $week['first']), 
        l(date('d/m/Y', $week['last']), 'diet/food_cart/cart/' . $fc['fcid'] . '/' . $w, array('attributes' => array('rel' => 'lightframe[|width:370px; height:585px;]'))),
      );
    }
    $header = array(
      t('Instance'),
      t('Initial date'),
      t('Final date'),
    );
    $rows[] = array('', array('data' => theme('table', $header, $rows2), 'colspan' => 2));
  }
  $header = array(
    t('Food cart'),
    t('Initial date'),
    t('Final date'),
  );
  $output .= theme('table', $header, $rows);
  
  return $output;
}

/**
 * @} End of defgroup diet_theme.
 */